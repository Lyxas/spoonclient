import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

//Le store va permettre

export default new Vuex.Store({
  state: {
    //You must set your api token/key here
    api:"",
    lastResult: {},
    filters: { // contient la selection des différents filtres
      intolerances: [],
      cuisines: [],
      excludeCuisine:[],
      diet:[]
    }
  },
  getters:{
    getLastResult: (state) =>{
      return state.lastResult;
    },
    getIntolerences: (state)=>{
      return state.filters.intolerances
    }
  },
  mutations: {
    updateFilter(state, payload){ // met à jours les filtres séléctionnés
      state.filters[payload.name] = payload.value
    }
  },
  actions: {
  },
  modules: {
  }
})
